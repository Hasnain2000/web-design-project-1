<?php require('includes/config.php'); ?>



<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page['about']['title'].' - '.$Website['name']; ?></title>
		<?php require('includes/links.php'); ?>
</head>
<body>
<?php require('includes/header.php');?>

<section class="bg-header-all">

<div class="header-content" >
	<div class="header-item text-center">
		<h1><?php echo $page['about']['header']['title']; ?><hr class="heading-border-bottom"></h1>
		<a href="#contact-form" class="btn-primary"><?php echo $page['about']['button']['Text'];?></a>
	</div>

</div>



</section>

<section class="Custom-Section" id="contact-form">
	<div class="Section-item  Custom-Section-Img">
		<img src="<?php echo $base_url.$page['about']['welcome']['image']['Url'];?>" class="animate-beat" alt="header image">	
	</div>
	<div class="Section-item">
		<h1><?php echo $page['about']['heading']['Text'];?> </h1>
		<p><?php echo $page['about']['Description']['Text'];?></p>
	</div>
	
</section>


<section class="our-team">
	<h1><?php echo $page['about']['our-team']['heading']; ?></h1><hr class="heading-border-bottom">
	<div class="members">
		<?php foreach ($page['about']['our-team']['member'] as $key => $members) {?>
			<div class="card">
				<div class="img">
					<img src="<?php echo $base_url.$members['image']; ?>" alt="member">
				</div>
				<h3 class="heading"><?php echo $members['Name']; ?></h3>
				<h6 class="sub-heading"><?php echo $members['designation'];?></h6><hr class="heading-border-bottom">
				<p class="body"><?php echo $members['Description'];?></p>
				<div class="social"><a href="<?php echo $members['follow']['url'];?>" class="btn-primary">Follow Me</a></div>
			</div>
		<?php }?>		
	</div>
</section>




<section class="technology">
	<h3><?php echo $page['about']['heading']['Technology']; ?></h3><hr class="heading-border-bottom">
	<div class="text-center">
		<?php foreach ($page['about']['Technology'] as $key => $Lang) {?>
			<button class="btn-secondary"><?php echo $Lang['Text']; ?></button>	
		<?php } ?>		
	</div>
</section>



<section class="Contact-Banner-Section" >
<div class="Banner responsive-flex-card">
	<div class="section-item">
		<h1><?php echo $page['contact']['Banner']['heading'];?></h1>
		<p><?php echo $page['contact']['Banner']['Text'];?></p>
		<a href="<?php echo $base_url.$nav[4]['url'];?>" class="btn-secondary"> <?php echo $page['contact']['Banner']['Button']['Text'];?> </a>
	</div>
	<div class="section-item section-item-img">
		<img src="<?php echo $page['contact']['Banner']['Image']; ?>" class="animate-beat">
	</div>
</div>
</section>


 
<?php require('includes/footer.php'); ?>
</body>
</html>