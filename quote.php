<?php require('includes/config.php');

require('includes/functions.php');?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page['quote']['title'].' - '.$Website['name']; ?></title>
		<?php require('includes/links.php'); ?>
</head>
<body>
<?php require('includes/header.php'); ?>

<section class="bg-header-all">
<div class="header-content" >
	<div class="header-item text-center">
		<h1><?php echo $page['quote']['welcome']['title']; ?></h1>
	</div>

</div>
</section>




<section class="contact container" id="contact-form">
	<div class="contact-item contact-item-img">
		<img src="<?php echo $base_url.$page['quote']['form']['image']['url']?>" class="animate-beat" alt="contact image">	
	</div>
	<div class="contact-item">
		<h1><?php echo $page['quote']['form']['Text'];?><hr class="heading-border-bottom-no-margin"></h1>
		<form  method="post"  >
			<?php 
				if(isset($_POST['quote-form'])){
					$response=QuoteFormFunction($_POST['name'],$_POST['email'],$_POST['phone'],$_POST['cat'],$_POST['budget'],$_POST['timeperiod'],$_POST['description']);
				if(count($response)==0){
					$data='Quotation Response'.PHP_EOL.' Date : '.date("F j, Y, g:i a").PHP_EOL.' Name : '.$_POST['name'].PHP_EOL.' Email : '.$_POST['email'].PHP_EOL.' Phone : '.$_POST['phone'].PHP_EOL.' Category : '.$_POST['cat'].PHP_EOL.' Budget : '.$_POST['budget'].PHP_EOL.' Timeline : '.$_POST['timeperiod'].PHP_EOL.' Description : '.$_POST['description'];
					$fp = fopen('response/quote/'.time().'.txt','a');
					fwrite($fp, $data);
					fclose($fp);

					?>
					<div class="alert-success">
						<h3 class="text-success">Thank you. We will get back to you with Project Report.</h3>
						<a href="<?php echo $nav[0]['url']; ?>" class="btn-secondary">Back to Home</a>
					</div>


				<?php }else{
					if(isset($response) && in_array("name",$response)){
						echo '<div class="form-group">
								<input type="text" class="form-has-error" value="'.SanatizeData($_POST['name']).'" name="name" placeholder="Name" autocomplete="off" required="required">
								<small class="text-error">Name only contain Letter and spaces.</small>
							</div>';
					}else{
						echo '<div class="form-group">
								<input type="text" name="name" value="'.SanatizeData($_POST['name']).'" placeholder="Name" autocomplete="off" required="required">
							</div>';}


					if(isset($response) && in_array("email",$response)){
						echo '<div class="form-group">
								<input type="email" class="form-has-error" value="'.SanatizeData($_POST['email']).'" name="email" placeholder="Email" autocomplete="off" required="required">
								<small class="text-error">Please Provide a valid email address</small>
							</div>';
					}else{
						echo '<div class="form-group">
								<input type="email" name="email" value="'.SanatizeData($_POST['email']).'" placeholder="Email" autocomplete="off" required="required">
							</div>';}

					if(isset($response) && in_array("phone",$response)){
						echo '<div class="form-group">
								<input type="tel" class="form-has-error" name="phone" pattern="^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$" placeholder="Phone No(+923xxxxxxxxx)" value="'.SanatizeData($_POST['phone']).'" autocomplete="off" required="required">
								<small class="text-error">Please Provide a valid Phone eg. +923325856766</small>
							</div>';
					}else{
						echo '<div class="form-group">
								<input type="tel" name="phone" value="'.SanatizeData($_POST['phone']).'" pattern="^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$" placeholder="Phone No(+923xxxxxxxxx)" autocomplete="off" required="required">
							</div>';}

					if(isset($response) && in_array("category",$response)){
						echo '<div class="form-group">
										<select name="cat" class="form-has-error" required><option selected="selected" disabled="disabled">What you want to design?</option>
										<option value="Mobile Development">Mobile Development</option>
										<option value="Web and App Development">Web and App Development</option>
										<option value="Graphic Design">Graphic Design</option></select><small class="text-error">Invalid category</small>
										</div>';
					}else{
						if($_POST['cat']=='Mobile Development'){
										echo '<div class="form-group">
											<select name="cat" required><option disabled="disabled" >What you want to design?</option>
											<option value="Mobile Development" selected="selected">Mobile Development</option>
											<option value="Web and App Development">Web and App Development</option>
											<option value="Graphic Design">Graphic Design</option></select>
										</div>';
									}else if($_POST['cat']=='Web and App Development'){
										echo '<div class="form-group">
											<select name="cat" required><option  disabled="disabled">What you want to design?</option>
											<option value="Mobile Development">Mobile Development</option>
											<option value="Web and App Development" selected="selected">Web and App Development</option>
											<option value="Graphic Design">Graphic Design</option></select>
										</div>';
									}else if($_POST['cat']=='Graphic Design'){
										echo '<div class="form-group">
											<select name="cat" required><option  disabled="disabled">What you want to design?</option>
											<option value="Mobile Development">Mobile Development</option>
											<option value="Web and App Development">Web and App Development</option>
											<option value="Graphic Design" selected="selected">Graphic Design</option></select>
										</div>';}}

					
					if(isset($response) && in_array("budget",$response)){
						echo '<div class="form-group">
								<input type="number" class="form-has-error" name="budget"  value="'.SanatizeData($_POST['budget']).'" placeholder="Estimated Budget ($)" min="1" autocomplete="off" required="required">
								<small class="text-error">Please Provide a valid Budget.(Min 1$)</small>
							</div>';
					}else{
						echo '<div class="form-group">
								<input type="number" name="budget" value="'.SanatizeData($_POST['budget']).'" placeholder="Estimated Budget ($)" min="1" autocomplete="off" required="required">
							</div>';}


					if(isset($response) && in_array("timeperiod",$response)){
						echo '<div class="form-group">
								<input type="number" class="form-has-error" name="timeperiod"  value="'.SanatizeData($_POST['timeperiod']).'" placeholder="Timeline (in days)" min="1" autocomplete="off" required="required">
								<small class="text-error">Please Provide a valid timeline.(Min 1 day)</small>
							</div>';
					}else{
						echo '<div class="form-group">
								<input type="number" name="timeperiod"  value="'.SanatizeData($_POST['timeperiod']).'" placeholder="Timeline (in days)" min="1" autocomplete="off" required="required">
							</div>';}

					if(isset($response) && in_array("description",$response)){
						echo '<div class="form-group">
								<textarea name="description" class="form-has-error" placeholder="Project Detail/Statement" rows="5" autocomplete="off" required="required">'.SanatizeData($_POST['description']).'</textarea>
								<small class="text-error">Please Provide a valid description</small>
							</div>';
					}else{
						echo '<div class="form-group">
								<textarea name="description"  placeholder="Project Detail/Statement" rows="5" autocomplete="off" required="required">'.SanatizeData($_POST['description']).'</textarea>
							</div>';}

					echo '<div class="form-group text-center">
							<input type="submit" name="quote-form" class="btn-primary" value="Send">
						</div>';	

				}
			}else{
			 ?> 
			 <div class="form-group">
				<input type="text" name="name" placeholder="Name" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<input type="email" name="email" placeholder="Email" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<input type="tel" name="phone" pattern="^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$" placeholder="Phone No(+923xxxxxxxxx)" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				
					<?php 
							if(isset($_GET['cat'])){
									if($_GET['cat']=='Mobile Development'){
										echo '<select name="cat" required><option disabled="disabled" >What you want to design?</option>
										<option value="Mobile Development" selected="selected">Mobile Development</option>
										<option value="Web and App Development">Web and App Development</option>
										<option value="Graphic Design">Graphic Design</option></select>';
									}else if($_GET['cat']=='Web and App Development'){
										echo '<select name="cat" required><option  disabled="disabled">What you want to design?</option>
										<option value="Mobile Development">Mobile Development</option>
										<option value="Web and App Development" selected="selected">Web and App Development</option>
										<option value="Graphic Design">Graphic Design</option></select>';
									}else if($_GET['cat']=='Graphic Design'){
										echo '<select name="cat" required><option  disabled="disabled">What you want to design?</option>
										<option value="Mobile Development">Mobile Development</option>
										<option value="Web and App Development">Web and App Development</option>
										<option value="Graphic Design" selected="selected">Graphic Design</option></select>';
									}else{
										echo '<select name="cat" class="form-has-error" required><option selected="selected" disabled="disabled">What you want to design?</option>
										<option value="Mobile Development">Mobile Development</option>
										<option value="Web and App Development">Web and App Development</option>
										<option value="Graphic Design">Graphic Design</option></select><small class="text-error">Invalid category</small>';
									}
							}else{
								echo '<select name="cat" required><option selected="selected" disabled="disabled">What you want to design?</option>
										<option value="Mobile Development">Mobile Development</option>
										<option value="Web and App Development">Web and App Development</option>
										<option value="Graphic Design">Graphic Design</option></select>';
							}

					?>

				
			</div>
			<div class="form-group">
				<input type="number" name="budget" placeholder="Estimated Budget ($)" min="1" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<input type="number" name="timeperiod" placeholder="Timeline (in days)" min="1" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<textarea name="description" placeholder="Project Detail/Statement" rows="5" autocomplete="off" required="required"></textarea>
			</div>
			<div class="form-group text-center">
				<input type="submit" name="quote-form" class="btn-primary" value="Send">
			</div>

		<?php } ?>
		</form>
	</div>
	
</section>





<?php require('includes/footer.php'); ?>
</body>
</html>