<?php require('includes/config.php');

require('includes/functions.php');?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page['contact']['title'].' - '.$Website['name']; ?></title>
		<?php require('includes/links.php'); ?>
</head>
<body>
<?php require('includes/header.php'); ?>

<section class="bg-header-all">
<div class="header-content" >
	<div class="header-item text-center">
		<h1><?php echo $page['contact']['header']['title']; ?></h1>
		<a href="#contact-form" class="btn-primary"><?php echo $page['contact']['button']['Text'];?></a>
	</div>

</div>
</section>




<section class="contact container" id="contact-form">
	<div class="contact-item contact-item-img">
		<img src="<?php echo $base_url.$page['contact']['form']['image']['url'];?>" class="animate-beat" alt="contact image">	
	</div>
	<div class="contact-item">
		<h1><?php echo $page['contact']['form']['Text'];?><hr class="heading-border-bottom-no-margin"></h1>

		<form  method="post" >
			<?php 
			if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message'])){
				


				$response=ContactFormFunction($_POST['name'],$_POST['email'],$_POST['subject'],$_POST['message']);
			if(count($response)==0){
				$data='Contact US Response'.PHP_EOL.' Date : '.date("F j, Y, g:i a").PHP_EOL.' Name : '.$_POST['name'].PHP_EOL.' Email : '.$_POST['email'].PHP_EOL.' Subject : '.$_POST['subject'].PHP_EOL.' Message : '.$_POST['message'];
					$fp = fopen('response/contact/'.time().'.txt','a');
					fwrite($fp, $data);
					fclose($fp);

				?>
			<div class="alert-success">
				<h3 class="text-success">Thank you for contacting Us. We will get you as soon as possible</h3>
				<a href="<?php echo $nav[0]['url']; ?>" class="btn-secondary">Back to Home</a>
			</div>
			<?php }else{
				if(isset($response) && in_array("name",$response)){
					echo '<div class="form-group">
							<input type="text" class="form-has-error" value="'.SanatizeData($_POST['name']).'" name="name" placeholder="Name" autocomplete="off" required="required">
							<small class="text-error">Name only contain Letter and spaces.</small>
						</div>';
				}else{
					echo '<div class="form-group">
							<input type="text" name="name" value="'.SanatizeData($_POST['name']).'" placeholder="Name" autocomplete="off" required="required">
						</div>';}

				if(isset($response) && in_array("email",$response)){
					echo '<div class="form-group">
							<input type="email" class="form-has-error" value="'.SanatizeData($_POST['email']).'" name="email" placeholder="Email" autocomplete="off" required="required">
							<small class="text-error">Please Provide a valid email address</small>
						</div>';
				}else{
					echo '<div class="form-group">
							<input type="email" name="email" value="'.SanatizeData($_POST['email']).'" placeholder="Email" autocomplete="off" required="required">
						</div>';}
				?>
				<div class="form-group">
					<input type="text" name="subject" value="<?php echo SanatizeData($_POST['subject']); ?>" placeholder="Subject" autocomplete="off" required="required">
				</div>
				<div class="form-group">
					<textarea name="message" placeholder="Message" rows="5" autocomplete="off"><?php echo SanatizeData($_POST['message']); ?></textarea required="required">
				</div>
				<div class="form-group text-center">
					<input type="submit" class="btn-primary" value="Send">
				</div>


			 <?php }/*error condition*/}/*if Postmethod is set*/else{ ?>

			 <div class="form-group">
				<input type="text" name="name" placeholder="Name" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<input type="email" name="email" placeholder="Email" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<input type="text" name="subject" placeholder="Subject" autocomplete="off" required="required">
			</div>
			<div class="form-group">
				<textarea name="message" placeholder="Message" rows="5" autocomplete="off"></textarea required="required">
			</div>
			<div class="form-group text-center">
				<input type="submit" class="btn-primary" value="Send">
			</div>



			 	<?php }?>
		</form>
	</div>
	
</section>



<?php require('includes/footer.php'); ?>
</body>
</html>