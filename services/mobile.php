<?php require('../includes/config.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $Services[0]['title'].' - '.$Website['name']; ?></title>
		<?php require('../includes/links.php'); ?>
</head>
<body>
<?php require('../includes/header.php');  ?>






<section class="bg-header-all">
<div class="header-content" >
	<div class="header-item text-center">
		<h1><?php echo $Services[0]['header']['title']; ?> - Service</h1>
	</div>

</div>
</section>


<section class="Custom-Section">
		<div class="Section-item  Custom-Section-Img">
			<img src="<?php echo $base_url.$page['home'][0]['services']['image'];?>" class="animate-beat" alt="Service">	
		</div>
		<div class="Section-item">
			<h1><?php echo $nav[1]['child'][0]['title'];?> <hr class="heading-border-bottom-no-margin"><hr class="heading-border-bottom-no-margin-p-5"></h1>
			<p><?php echo $page['home'][0]['services']['Description'];?></p><br>
			<a href="<?php echo $base_url.$Services[0]['header']['quote']['page']['url'];?>" class="btn-primary"><?php echo $Services[0]['header']['quote']['btn']['Text']; ?></a>
		</div>
</section>





<section class="technology">
	<h3><?php echo $Services[0]['Text']['Technology']; ?></h3><hr class="heading-border-bottom">
	<div class="text-center">
		<?php foreach ($Services[0]['Technology'] as $key => $Lang) {?>
			<button class="btn-secondary"><?php echo $Lang['Text']; ?></button>	
		<?php } ?>		
	</div>
</section>



<section class="Contact-Banner-Section" >
<div class="Banner responsive-flex-card">
	<div class="section-item">
		<h1><?php echo $page['contact']['Banner']['heading'];?></h1>
		<p><?php echo $page['contact']['Banner']['Text'];?></p>
		<a href="<?php echo $base_url.$nav[4]['url'];?>" class="btn-secondary"> <?php echo $page['contact']['Banner']['Button']['Text'];?> </a>
	</div>
	<div class="section-item section-item-img">
		<img src="<?php echo $base_url.$page['contact']['Banner']['Image']; ?>" class="animate-beat">
	</div>
</div>
</section>




<?php require('../includes/footer.php'); ?>
</body>
</html>