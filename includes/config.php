<?php 
$Host = "http://$_SERVER[HTTP_HOST]";
$Folder="/Web/";
$base_url=$Host.$Folder;



/***************Website Settings Start*************/

$Website['logo']="graphics/logo.webp";
$Website['name']="SparkTech";
$Website['Copyright']='&copy; 2020 <a href="https://sparktech.cf" target="_blank">SparkTech</a>. All rights reserved.';
$Website['home']['page']['url']="index.php";
/***************Website Settings Ends*************/




/**************Services Start************************/
$Services[0]['title']="Mobile Development";
$Services[0]['url']="services/mobile.php";
$Services[0]['header']['title']="Mobile Development";
$Services[0]['header']['quote']['btn']['Text']="Send Us your Project Requirement";
$Services[0]['header']['quote']['page']['url']="quote.php?cat=Mobile Development";
$Services[0]['Text']['Technology']="We code in a broad range of technologies";
$Services[0]['Technology'][0]['Text']="Flutter";
$Services[0]['Technology'][1]['Text']="Java";
$Services[0]['Technology'][2]['Text']="C++";
$Services[0]['Technology'][3]['Text']="Python";
$Services[0]['Technology'][4]['Text']="Objective-C";


$Services[1]['title']="Website & App Development";
$Services[1]['url']="services/web.php";
$Services[1]['header']['title']="Website & App Development";
$Services[1]['header']['quote']['btn']['Text']="Send Us your Project Requirement";
$Services[1]['header']['quote']['page']['url']="quote.php?cat=Web and App Development";
$Services[1]['Text']['Technology']="We build the thing that are Beyond the limits";
$Services[1]['Technology'][0]['Text']="HTML";
$Services[1]['Technology'][1]['Text']="CSS";
$Services[1]['Technology'][2]['Text']="Javascript";
$Services[1]['Technology'][3]['Text']="Php";
$Services[1]['Technology'][4]['Text']="Laravel";
$Services[1]['Technology'][5]['Text']="React JS";
$Services[1]['Technology'][6]['Text']="Node JS";
$Services[1]['Technology'][7]['Text']="Python";
$Services[1]['Technology'][8]['Text']="asp.net";


$Services[2]['title']="Graphic Design";
$Services[2]['url']="services/graphics.php";
$Services[2]['header']['title']="Graphic Design";
$Services[2]['header']['quote']['btn']['Text']="Send Us your Project Requirement";
$Services[2]['header']['quote']['page']['url']="quote.php?cat=Graphic Design";
$Services[2]['Text']['Technology']="We use Advance Tools to Build Beautiful Layouts";
$Services[2]['Technology'][0]['Text']="Adobe Photoshop";
$Services[2]['Technology'][1]['Text']="Adobe Illustrator";
$Services[2]['Technology'][2]['Text']="Figma";
$Services[2]['Technology'][3]['Text']="PicsArt";
$Services[2]['Technology'][4]['Text']="ProofHub";
/**************Services End************************/




/**************Nav bar Start***********************/
$nav[0]['title']="Home";
$nav[0]['url']="index.php";

$nav[1]['title']="Services";
$nav[1]['url']="index.php#Our-Services";
$nav[1]['has-child']=true;
	$nav[1]['child'][0]['title']=$Services[0]['title'];
	$nav[1]['child'][0]['url']=$Services[0]['url'];
	$nav[1]['child'][1]['title']=$Services[1]['title'];
	$nav[1]['child'][1]['url']=$Services[1]['url'];
	$nav[1]['child'][2]['title']=$Services[2]['title'];
	$nav[1]['child'][2]['url']=$Services[2]['url'];




$nav[2]['title']="Portfolio";
$nav[2]['url']="portfolio.php";

$nav[3]['title']="About Us";
$nav[3]['url']="about.php";

$nav[4]['title']="Contact Us";
$nav[4]['url']="contact.php";


/**************Nav bar End***********************/










/**************Home Page Start***********************/
$page['home']['title']="Home ";
$page['home']['welcome']['Message']="Welcome To SparkTech";
$page['home']['welcome']['Text']="We are offering multiple services in Website Development, Mobile & App Development and Graphic Design";
$page['home']['welcome']['button']['Text']="Let's discuss your Project";
$page['home']['welcome']['button']['Url']="quote.php";
$page['home']['welcome']['image']['Url']="graphics/home-img-content.svg";






$page['home'][0]['services']['url']=$nav[1]['child'][0]['url'];
$page['home'][0]['services']['image']="graphics/mobile-apps-development.svg";
$page['home'][0]['services']['button Text']="Learn More";
$page['home'][0]['services']['Description']="We provide complete solutions, which enables businesses to leverage to gain sustainable competitive advantages in today’s marketplace. We are known for our talent, passion, work ethic and building ongoing long term relationships and commitment through support and maintenance. A good team is significant to the success of any business. Sparktech takes pride in having a trustworthy and sturdy team. Since our founding, we have worked with the best in the industry. Our turn-around time is the most minimal and we assure reply within 1 business day.Learn More";


$page['home'][1]['services']['url']=$nav[1]['child'][1]['url'];
$page['home'][1]['services']['image']="graphics/web-development.svg";
$page['home'][1]['services']['button Text']="Learn More";
$page['home'][1]['services']['Description']="We provide complete solutions, which enables businesses to leverage to gain sustainable competitive advantages in today’s marketplace. We are known for our talent, passion, work ethic and building ongoing long term relationships and commitment through support and maintenance. A good team is significant to the success of any business. Sparktech takes pride in having a trustworthy and sturdy team. Since our founding, we have worked with the best in the industry. Our turn-around time is the most minimal and we assure reply within 1 business day.Learn More";



$page['home'][2]['services']['url']=$nav[1]['child'][2]['url'];
$page['home'][2]['services']['image']="graphics/graphics-design.svg";
$page['home'][2]['services']['button Text']="Learn More";
$page['home'][2]['services']['Description']="We provide complete solutions, which enables businesses to leverage to gain sustainable competitive advantages in today’s marketplace. We are known for our talent, passion, work ethic and building ongoing long term relationships and commitment through support and maintenance. A good team is significant to the success of any business. Sparktech takes pride in having a trustworthy and sturdy team. Since our founding, we have worked with the best in the industry. Our turn-around time is the most minimal and we assure reply within 1 business day.Learn More";
/**************Home Page Ends***********************/









/**************Contact Page Start***********************/
$page['contact']['title']="Contact ";
$page['contact']['header']['title']="Have a Question?";
$page['contact']['button']['Text']="Get In Touch";
$page['contact']['welcome']['image']['Url']="graphics/home-img-content.svg";
$page['contact']['form']['Text']="Send Us Message";
$page['contact']['form']['image']['url']="graphics/contact-form.png";
$page['contact']['form']['action']['url']="";
/*********************banner Contact Page***************/
$page['contact']['Banner']['heading']="Want to Discuss your Project with US?";
$page['contact']['Banner']['Text']="Contact Us AnyTime. We are Available 24/7";
$page['contact']['Banner']['Image']="graphics/contactus-man.svg";
$page['contact']['Banner']['Button']['Text']="Contact Us";
/*********************banner Contact PageEnds***************/
/**************Contact Page Ends***********************/








/**************Quote Page Start***********************/
$page['quote']['title']="Send Quotation";
$page['quote']['welcome']['title']="Let's Discuss Your Project";
$page['quote']['welcome']['image']['Url']="graphics/home-img-content.svg";
$page['quote']['form']['Text']="Send Us Your Project Requirement";
$page['quote']['form']['image']['url']="graphics/quote-bg.svg";
/**************Quote Page Ends***********************/


























/**************About Page Start***********************/
$page['about']['title']="About";
$page['about']['header']['title']="Why Choose ".$Website['name']."?";
$page['about']['button']['Text']="Learn More";
$page['about']['welcome']['image']['Url']="graphics/about-us.png";
$page['about']['heading']['Text']="We Are Providing Best Development Services in the Industry";
$page['about']['Description']['Text']="We provide complete solutions, which enables businesses to leverage to gain sustainable competitive advantages in today’s marketplace. We are known for our talent, passion, work ethic and building ongoing long term relationships and commitment through support and maintenance. A good team is significant to the success of any business. <b>Sparktech</b> takes pride in having a trustworthy and sturdy team. Since our founding, we have worked with the best in the industry. Our turn-around time is the most minimal and we assure reply within 1 business day.";

$page['about']['mission']['main']['heading']="Our Mission";
$page['about']['mission']['heading']="We Are Providing Best Development Services in the Industry";
$page['about']['mission']['Description']['Text']="We provide complete business solutions, which enables businesses to leverage leading-edge technology to gain sustainable competitive advantages in today’s marketplace. We are known for our talent, passion, work ethic and building ongoing long term relationships and commitment through support and maintenance. A good team is significant to the success of any business. GEXTON takes pride in having a trustworthy and sturdy team of more than 30 members. Since our founding, we have worked with the best in the industry. Our turn-around time is the most minimal and we assure reply within 1 business day.";
$page['about']['mission']['Description']['image']['url']="";



$page['about']['our-team']['heading']="Our Team Member";

$page['about']['our-team']['member'][0]['image']="graphics/user.jpg";
$page['about']['our-team']['member'][0]['Name']="Hasnain Ali";
$page['about']['our-team']['member'][0]['designation']="Backend Developer";
$page['about']['our-team']['member'][0]['Description']="Let us be grateful to people who make us happy. they are the charming gardeners who make our souls blossom";
$page['about']['our-team']['member'][0]['follow']['url']="javascript:;";



$page['about']['our-team']['member'][1]['image']="graphics/user.jpg";
$page['about']['our-team']['member'][1]['Name']="Syed Shahrose Sohail";
$page['about']['our-team']['member'][1]['designation']="Technical Head";
$page['about']['our-team']['member'][1]['Description']="Let us be grateful to people who make us happy. they are the charming gardeners who make our souls blossom";
$page['about']['our-team']['member'][1]['follow']['url']="javascript:;";

$page['about']['our-team']['member'][2]['image']="graphics/user.jpg";
$page['about']['our-team']['member'][2]['Name']="Syed Khizar Abbas Naqvi";
$page['about']['our-team']['member'][2]['designation']="Support Head";
$page['about']['our-team']['member'][2]['Description']="Let us be grateful to people who make us happy. they are the charming gardeners who make our souls blossom";
$page['about']['our-team']['member'][2]['follow']['url']="javascript:;";



$page['about']['heading']['Technology']="We code in a broad range of technologies";
$page['about']['Technology'][0]['Text']="HTML";
$page['about']['Technology'][1]['Text']="CSS";
$page['about']['Technology'][2]['Text']="Javascript";
$page['about']['Technology'][3]['Text']="PHP";
$page['about']['Technology'][4]['Text']="Laravel";
$page['about']['Technology'][5]['Text']="React Js";
$page['about']['Technology'][6]['Text']="Asp.net";






/**************About Page Ends***********************/





/*********** Portfolio Page Start************/

$page['portfolio']['title']="Portfolio";
$page['portfolio']['heading']="Check out Some of Our Projects";
$page['portfolio']['button']['Text']="Explore";


$page['portfolio']['websites'][0]['Title']="Evor Gaming";
$page['portfolio']['websites'][0]['Description']="The project is related to gaming Industry. We were asked to built a Website which can be used for management of Online gaming Tournaments. The main language used in the Development is Php Framework(Laravel)";
$page['portfolio']['websites'][0]['Image']="graphics/portfolio/1.webp";
$page['portfolio']['websites'][0]['Url']="https://evorgaming.com/";

$page['portfolio']['websites'][1]['Title']="Shop Pom Pom";
$page['portfolio']['websites'][1]['Description']="ShopPomPom is a Company which provide a solution for plastic free product. Its an Ecommerce Website & main core language used is liquid.";
$page['portfolio']['websites'][1]['Image']="graphics/portfolio/2.png";
$page['portfolio']['websites'][1]['Url']="https://shoppompom.com/";

$page['portfolio']['websites'][2]['Title']="CureSee";
$page['portfolio']['websites'][2]['Description']="CureSee is a comprehensive software for Binocular Vision Assessment & vision Therapy for Amblyopia, Asthenopia, etc. It provide patients a platform to find Qualified Doctor in their locality";
$page['portfolio']['websites'][2]['Image']="graphics/portfolio/3.png";
$page['portfolio']['websites'][2]['Url']="https://curesee.com/";


$page['portfolio']['websites'][3]['Title']="Cashlink Finance";
$page['portfolio']['websites'][3]['Description']="Cashlink is a Core Six Axes Binary System (MLM) which provides user platform for investment and earn Money using a Referal Based System using Cryptocurrency as method of Payment.";
$page['portfolio']['websites'][3]['Image']="graphics/portfolio/4.png";
$page['portfolio']['websites'][3]['Url']="https://cashlink.finance/";



/*********** Portfolio Page Ends************/









