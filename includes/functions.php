<?php 


function SanatizeData($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }





function ContactFormFunction($name,$email,$subject,$message){
	$error=array();
	$name=SanatizeData($name);
	$email=SanatizeData($email);
	$subject=SanatizeData($subject);
	$message=SanatizeData($message);

	if(!ctype_alpha(str_replace(' ', '', $name))){
		array_push($error, 'name');}

	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			array_push($error, 'email');}
	return $error;
}





function QuoteFormFunction($name,$email,$phone,$category,$budget,$timeperoid,$description){
	$error=array();
	$name=SanatizeData($name);
	$email=SanatizeData($email);
	$phone=SanatizeData($phone);
	$category=SanatizeData($category);
	$budget=SanatizeData($budget);
	$timeperoid=SanatizeData($timeperoid);
	$description=SanatizeData($description);

	if(!ctype_alpha(str_replace(' ', '', $name))){
		array_push($error, 'name');}

	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			array_push($error, 'email');}

	if(!preg_match('/^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$/', $phone)){
			array_push($error, 'phone');}

	if(!("Mobile Development"==$category || "Web and App Development"==$category || "Graphic Design"==$category)){
			array_push($error, 'category');
		}

	if(is_numeric($budget) && (float)$budget<1){
			array_push($error, 'budget');}

	if(is_numeric($timeperoid) && (float)$timeperoid<1){
			array_push($error, 'timeperiod');}

	if($description==' '){
			array_push($error, 'description');}

	return $error;
}




?>