<header>
	<nav class="nav-bar container">
		<div class="logo">
			<a href="<?php echo $base_url.$Website['home']['page']['url']; ?>">
			<img src="<?php echo $base_url.$Website['logo']; ?>" width="157"></a>
		</div>
		
			<label for="toggle-mobile-menu" class="mobile-menu-toggle-container" >&#9776;</label>
		<input id="toggle-mobile-menu" type="checkbox" />
		
		<ul class="menu">
			<?php
				foreach ($nav as $key => $navbar) {
					if(isset($navbar['has-child'])){
						echo '<li class="has-child"><a href="'.$base_url.$navbar['url'].'">'.$navbar['title'].'</a><ul class="child">';
						foreach ($navbar['child'] as $key => $child_item) {
								echo '<li><a href="'.$base_url.$child_item['url'].'">'.$child_item['title'].'</a></li>';	}
						echo "</ul></li>";
					}else{echo '<li><a href="'.$base_url.$navbar['url'].'">'.$navbar['title'].'</a></li>';}
				}?>
		</ul>
		
	</nav>
</header>