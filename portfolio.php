<?php include('includes/config.php'); ?>



<!DOCTYPE html>
<html>
<head>
	<title><?php echo $page['portfolio']['title'].' - '.$Website['name']; ?></title>
		<?php include('includes/links.php'); ?>
</head>
<body>
<?php include('includes/header.php'); ?>




<section class="bg-header-all">
	<div class="header-content" >
		<div class="header-item text-center">
			<h1><?php echo $page['portfolio']['heading']; ?><hr class="heading-border-bottom"></h1>
			<a href="#portfolio" class="btn-primary"><?php echo $page['portfolio']['button']['Text'];?></a>
		</div>
	</div>
</section>



<section class="our-team portfolio-Section" id="portfolio">

	<div class="members">
		<?php foreach ($page['portfolio']['websites'] as $key => $members) {?>
			<div class="card">
				<img src="<?php echo $members['Image']; ?>">
				<h3 class="heading"><?php echo $members['Title']; ?></h3><hr class="heading-border-bottom">
				<p class="body"><?php echo $members['Description'];?></p>
				<div class="social"><a href="<?php echo $members['Url'];?>" target="_blank" class="btn-primary">Visit Site</a></div>
			</div>
		<?php }?>

			
	</div>
</section>




<section class="Contact-Banner-Section" >
<div class="Banner responsive-flex-card">
	<div class="section-item">
		<h1><?php echo $page['contact']['Banner']['heading'];?></h1>
		<p><?php echo $page['contact']['Banner']['Text'];?></p>
		<a href="<?php echo $base_url.$nav[4]['url'];?>" class="btn-secondary"> <?php echo $page['contact']['Banner']['Button']['Text'];?> </a>
	</div>
	<div class="section-item section-item-img">
		<img src="<?php echo $page['contact']['Banner']['Image']; ?>" class="animate-beat">
	</div>
</div>
</section>









 
<?php include('includes/footer.php'); ?>
</body>
</html>