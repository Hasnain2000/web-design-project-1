<?php require('includes/config.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page['home']['title'].' - '.$Website['name']; ?></title>
		<?php require('includes/links.php'); ?>
</head>
<body>
<?php require('includes/header.php');  ?>






<section class="bg-header">
	<div class="header-content" >
		<div class="header-item">
			<h1><?php echo $page['home']['welcome']['Message']; ?></h1>
			<p><?php echo $page['home']['welcome']['Text'];?></p>
			<a class="btn-primary" href="<?php echo $base_url.$page['home']['welcome']['button']['Url'];?>"><?php echo $page['home']['welcome']['button']['Text'];?></a>
		</div>
		<div class="header-item  header-item-svg">
			<img src="<?php echo $base_url.$page['home']['welcome']['image']['Url']; ?>" class="animate-beat">
		</div>

	</div>
	<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" class="bg-polygon-white"><polygon fill="white" points="0,100 100,30 100,100"></polygon></svg>	
</section>



<section class="Our-Services" id="Our-Services">
	<h1>Our Services<hr class="heading-border-bottom"></h1>
	


	<div class="Custom-Section">
		<div class="Section-item  Custom-Section-Img">
			<img src="<?php echo $base_url.$page['home'][0]['services']['image'];?>" class="animate-beat" alt="Service">	
		</div>
		<div class="Section-item">
			<h1><?php echo $nav[1]['child'][0]['title'];?> <hr class="heading-border-bottom-no-margin"><hr class="heading-border-bottom-no-margin-p-5"></h1>
			<p><?php echo $page['home'][0]['services']['Description'];?></p>
			<a href="<?php echo $base_url.$page['home'][0]['services']['url'];?>" class="btn-primary"><?php echo $page['home'][0]['services']['button Text']; ?></a>
		</div>
	</div>


	<div class="Custom-Section">
		<div class="Section-item">
			<h1><?php echo $nav[1]['child'][1]['title'];?> <hr class="heading-border-bottom-no-margin"><hr class="heading-border-bottom-no-margin-p-5"></h1>
			<p><?php echo $page['home'][1]['services']['Description'];?></p>
			<a href="<?php echo $base_url.$page['home'][1]['services']['url'];?>" class="btn-primary"><?php echo $page['home'][1]['services']['button Text']; ?></a>
		</div>
		<div class="Section-item  Custom-Section-Img">
			<img src="<?php echo $base_url.$page['home'][1]['services']['image'];?>" class="animate-beat" alt="Service">	
		</div>
	</div>





	<div class="Custom-Section">
		<div class="Section-item  Custom-Section-Img">
			<img src="<?php echo $base_url.$page['home'][2]['services']['image'];?>" class="animate-beat" alt="Service">	
		</div>
		<div class="Section-item">
			<h1><?php echo $nav[1]['child'][2]['title'];?> <hr class="heading-border-bottom-no-margin"><hr class="heading-border-bottom-no-margin-p-5"></h1>
			<p><?php echo $page['home'][2]['services']['Description'];?></p>
			<a href="<?php echo $base_url.$page['home'][2]['services']['url'];?>" class="btn-primary"><?php echo $page['home'][2]['services']['button Text']; ?></a>
		</div>
	</div>


</section>




<section class="Contact-Banner-Section" >
<div class="Banner responsive-flex-card">
	<div class="section-item">
		<h1><?php echo $page['contact']['Banner']['heading'];?></h1>
		<p><?php echo $page['contact']['Banner']['Text'];?></p>
		<a href="<?php echo $base_url.$nav[4]['url'];?>" class="btn-secondary"> <?php echo $page['contact']['Banner']['Button']['Text'];?> </a>
	</div>
	<div class="section-item section-item-img">
		<img src="<?php echo $page['contact']['Banner']['Image']; ?>" class="animate-beat">
	</div>
</div>
</section>


<?php require('includes/footer.php'); ?>
</body>
</html>